.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:


.. README =============================================================

.. This project most likely has it's own README. We include it here.

.. toctree::
   :maxdepth: 2
   :caption: Readme

   ../../README

.. COMMUNITY SECTION ==================================================

..

.. toctree::
  :maxdepth: 2
  :caption: Collection ska_cicd_docker_base
  :hidden:

  collections/ska_cicd_docker_base

.. toctree::
  :maxdepth: 2
  :caption: Collection ska_cicd_k8s
  :hidden:

  collections/ska_cicd_k8s


.. toctree::
  :maxdepth: 2
  :caption: Collection ska_cicd_stack_cluster
  :hidden:

  collections/ska_cicd_stack_cluster



Ansible Common Roles
====================

The following is the list of common Ansible Role Collections used by the Systems Team:

- :doc:`collections/ska_cicd_docker_base`
- :doc:`collections/ska_cicd_k8s`
- :doc:`collections/ska_cicd_stack_cluster`
